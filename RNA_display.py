from RNA_rcnstr import RNA_reconstruction
from random import randint, choice

'''######################################################################################
Precondition: 
    (NONE)
Postcondition:
    returns entry.upper():
        entry: string representing a menu selection; it is made all upper case before
        have its value returned to avoid unnecessary errors
######################################################################################'''
def menu():
    print '[RNA Reconstruction Menu]'
    print '\t[1] Reconstructs chains for 3 given fragmented pairs'
    print '\t[2] Reconstructs a randomly generated chain'
    print '\t[3] Reconstructs a user entered chain'
    print '\t[Q] Quit'
    entry = raw_input('Enter a command: ')
    print '\n'
    return entry.upper()


'''######################################################################################
Precondition: 
    all_chains: set[string] representing all possible RNA chains to be constructed
                from the given fragments
    *given:
        len(given) == 1:    given[0]:   string of a complete RNA chain
        len(given) == 2:    given[0]:   list[string] representing a RNA chain
                                        fragmented by the G-enzyme
                            given[1]:   list[string] representing a RNA chain
                                        fragmented by the UC-enzyme
Postcondition:
    (NO RETURN VALUE)
    Outputs all possible RNA chain reconstructions given initial values
######################################################################################'''
def results(all_chains, *given):
    if len(given) > 1:
        print "The given enzyme fragmented chains:"
        print "\t[G-Enzyme]: ",given[0]
        print "\t[UC-Enzyme]: ",given[1]
    else:
        print "The given RNA chain:"
        print "\t",given[0]
    print "Can be reconstructed into the following RNA chains:"
    for x in all_chains:
        print "\t",x
    raw_input('*PRESS ENTER TO CONTINUE*')
    print '\n'


'''######################################################################################
Precondition: 
    user_chain: string that is entered by the user representing a complete RNA chain
Postcondition:
    returns 0 or 1:
        0 represents an invalid RNA chain (i.e. contains a character other than AUCG)
        1 represents a valid RNA chain entry
######################################################################################'''
def verify_chain(user_chain):
    for x in user_chain.upper():
        if x not in 'AUCG':
            return 0
    return 1


'''######################################################################################
Precondition: 
    (NONE)
Postcondition:
    (NO RETURN VALUE)
    Outputs the possible RNA chain reconstructions for 3 examples of fragmented pairs
######################################################################################'''
def examples():
    example_1 = [['AUCG','AUG','G','CU','ACUAUACG'],['GGAC','U','AU','GAU','C','U','AC','GC','AU']]
    results(RNA_reconstruction(example_1[0], example_1[1]), example_1[0], example_1[1])
    example_2 = [['G','CAG','G','G','AACG','AG','UG','ACAAC'],['GC','AGGGAAC','GAGU','GAC','AAC']]
    results(RNA_reconstruction(example_2[0], example_2[1]), example_2[0], example_2[1])
    example_3 = [['ACCG','G','G','ACG','CG','G','UUCUAC'],['AC','C','GGGAC','GC','GGU','U','C','U','AC']]
    results(RNA_reconstruction(example_3[0], example_3[1]), example_3[0], example_3[1])
    

'''######################################################################################
Precondition: 
    (NONE)
Postcondition:
    (NO RETURN VALUE)
    Outputs the possible RNA chain reconstructions for a randomly generated chain
######################################################################################'''
def generate_random():
    r_chain = random_chain()
    results(RNA_reconstruction(r_chain), r_chain)

    
'''######################################################################################
Precondition: 
    (NONE)
Postcondition:
    returns string representing a randomly constructed RNA chain of length 2 to 25
######################################################################################'''
def random_chain():
    chain = [choice('AUCG') for x in range(0, randint(2,25))]
    return ''.join(chain)


'''######################################################################################
Precondition: 
    (NONE)
Postcondition:
    (NO RETURN VALUE)
    outputs the possible RNA chain reconstructions for a user entered chain
######################################################################################'''
def user_defined():
    user_chain = raw_input('Enter a RNA chain: ')
    while not verify_chain(user_chain):
        print "INVALID CHAIN: RNA chains may only contain the characters AUCG"
        user_chain = raw_input('Enter a RNA chain: ')
    results(RNA_reconstruction(user_chain.upper()), user_chain.upper())