#**RNA Chain Reconstructor**#

## Author: Travis Geeting ##

### **Description:** ###
This program simulates the method of finding possible RNA chain reconstructions
    as stated in the document **RNA-Chain-Reconstruction.pdf** also included in
    this folder. It performs using three different modes: Example Output,
    Random Chain, and User Chain.
                
    *Example Output*- Outputs the possible RNA reconstructions for 3 different
                    examples of RNA chains that have been fragmented by the
                    G- and UC-Enzymes. The first example is that of the
                    fragmented pair used in the aforementioned document;
                    the other two are ones which I have constructed
                                
    *Random Chain*-   Outputs the possible RNA reconstructions for a randomly
                    generated complete RNA chain. The chain can be of length 2 to
                    25. The chain is complete, so it is first broken into two
                    fragmented lists; one is fragmented via the G-enzyme, the other
                    by the UC-enzyme. It is then run through the same algorithm as
                    the examples.
                
    *User Chain*-     Outputs the possible RNA reconstructions for a user entered
                    complete RNA chain. The chain is first checked to only contain
                    the letters AUCG. It is then evaluated in the same manner as
                    the Random Chain.


### Instructions: ###
    To run this program, navigate the directory to this file on the command line (terminal)
    and execute it with the following command:
    
                    python RNA_main.py
    
    The program will display a menu with options to enter 1,2,3, or Q. Entering 1 will run
    the Example Output. Entering 2 will run the Random Chain. Entering 3 will run the User
    Chain. Q will cause the program to end. The menu will continue displaying until a Q is
    entered. The message PRESS ENTER TO CONTINUE will display after every output of RNA
    reconstructions; this is for better viewability of the result, otherwise the menu
    would push it upwards on the screen. In the Example Output mode this will happen
    3 times before the menu reappears that way the results can be viewed one at a time.


### Additional Notes: ###
    *Visuals*-    
            Included in this file are multidigraph representations of the
            3 examples conducted in the program, which are also noted with
            their outputs. Respectively named *Example-1.pdf*, *Example-2.pdf*,
            and *Example-3.pdf*
                
    *Shortcomings*-
        Although this program is mostly successful in its simulation of the method,
        it is not entirely optimal or cleanly written. For instance, the generated
        multidigraph performs eulerian path search on nearly all noneulerian paths.
        Also given that a vertex in the multidigraph has multiple arcs to another
        vertex, all with the same or nonexistant interior bases, duplicate RNA
        chain reconstructions will be added to the list of total chains. The
        algorithm "cheats" and takes advantage of python's set data structure
        which removes duplicates when the list is type casted as a set. There is
        also the case in which the starting vertex does not match any vertex
        created in the digraph, such is the case with "ACGCGC". However, under
        these circumstances, the method can only recreate the initial chain anyway,
        so the algorithm "cheats" again by returning only this chain.