from copy import deepcopy
from itertools import chain

'''######################################################################################
Precondition: 
    ext_bases:  list(list(string)) representing all extended bases found through
                application of two enzymes in both sequences
Postcondition:
    returns all_bases:
        all_bases:  list(list(string)) where index [0] is a list of all individual bases
                    and index [1] is a list of all interior bases
######################################################################################'''
def find_all_bases(ext_bases):
    single_bases = []
    interior_bases = []
    for frag in ext_bases:
        if len(frag) == 1:
            single_bases.append(frag[0]) #stores all individual bases
        if len(frag) > 2:
            for x in frag[1:len(frag)-1]:
                interior_bases.append(x) #stores interior bases if they exist
    all_bases = [single_bases, interior_bases] #organizes both bases into a list
    return all_bases


'''######################################################################################
Precondition: 
    ext_bases:  list(list(string)) representing all extended bases found through
                application of two enzymes in both sequences
Postcondition:
    returns RNA_ends:
        RNA_ends:   list(string) of size 2 containing the start and end bases of
                    all possible RNA chain reconstructions
######################################################################################'''
def find_ends(ext_bases):
    all_bases = find_all_bases(ext_bases)
    all_single_bases = all_bases[0]
    all_interior_bases = all_bases[1]
    RNA_ends = []
    for x in all_single_bases:
        if x not in RNA_ends:
            if all_single_bases.count(x) > all_interior_bases.count(x):
                RNA_ends.append(x) #saves end bases when determined
    if len(RNA_ends) == 1:
        RNA_ends.append(RNA_ends[0]) #if start and end are the same, double the list
    return RNA_ends


'''######################################################################################
Precondition: 
    ext_bases:  list(list(string)) representing all extended bases found through
                application of two enzymes in both sequences
    RNA_ends:   list(string) of size 2 containing the start and end bases of
                    all possible RNA chain reconstructions
Postcondition:
    returns RNA_graph:
        RNA_graph:  dict(string:list(list(str))) representing the multidirected graph
                    constructed from the extended bases of the RNA chain; for the
                    (key, value) pair the key represents every vertex in the graph and
                    each separate list value is the adjacent arc where the last indexed
                    variable is the adjacent vertex and every previous value is an
                    interior base
######################################################################################'''
def construct_RNA_graph(ext_bases, RNA_ends):
    RNA_graph = {}
    for x in ext_bases:
        if len(x) > 1:
            if x[0] not in RNA_graph:
                RNA_graph[x[0]] = [x[1:len(x)]] #creates new vertex if base not in graph
            else:
                RNA_graph[x[0]].append(x[1:len(x)]) #otherwise adds new arc
        else:
            if x[0] not in RNA_graph:
                RNA_graph[x[0]] = [] #adds individual base to graph with no arcs
    label_ends(RNA_ends, RNA_graph) #reevaluates the end vertices
    return RNA_graph
    
    
'''######################################################################################
Precondition: 
    RNA_ends:   list(string) of size 2 containing the start and end bases of
                all possible RNA chain reconstructions
    RNA_graph:  dict(string:list(list(str))) representing the directed graph
                    constructed from the extended bases of the RNA chain
                    (See construct_RNA_graph header for full description)
Postcondition:
    (NO RETURN VALUE)
    RNA_ends is sorted so that index [0] is the starting vertex of RNA_graph and
    index [1] is the end vertex
######################################################################################'''
def label_ends(RNA_ends, RNA_graph):
    if not RNA_graph[RNA_ends[0]]: #the end vertex will have no arcs
        token = RNA_ends[0]
        RNA_ends[0] = RNA_ends[1]
        RNA_ends[1] = token   


'''######################################################################################
Precondition: 
    RNA_graph:  dict(string:list(list(str))) representing the directed graph
                    constructed from the extended bases of the RNA chain
                    (See construct_RNA_graph header for full description)
    vertex: string representing a vertex in the graph which is currently being evaluated
    curr_chain: string representing the RNA chain currently being reconstructed
    chain_length: integer representing the known length the original RNA chain must be
Postcondition:
    returns set(strings) of all possible RNA chain reconstructions using the graph
######################################################################################'''
def possible_RNA_chains(RNA_graph, vertex, curr_chain, chain_length):
    all_chains = []
    if not RNA_graph[vertex]: #checks for terminal vertex
        if not (len(curr_chain) < chain_length):
            return [curr_chain] #returns a complete chain verified by length
        else:
            return [] #returns empty if chain is prematurely ended
    else:
        for x in RNA_graph[vertex]: #performs pseudo eulerian path exhaustion
            y = deepcopy(x)         #"pseudo" being that it sometimes ends prematurely
            z = deepcopy(RNA_graph)
            z[vertex].remove(y)
            all_chains.append(possible_RNA_chains(z, y[len(y)-1], curr_chain+''.join(y), chain_length))
        return set(list(chain(*all_chains))) #changing list to type set removes duplicates