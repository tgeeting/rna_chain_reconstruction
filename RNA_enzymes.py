'''######################################################################################
Precondition: 
    chain:  string representing an RNA chain containing the characters [A, U, C, G]   
    enzyme: list(string) representing the enzyme being applied to the RNA chain; could be
            either G-enzyme or UC-enzyme
Postcondition:
    returns fragments:
        fragments:  list(string) representing an RNA chain now fragmented via enzyme
######################################################################################'''
def single_enzyme(chain, enzyme):
    fragments = []
    token = []
    itr = 0
    for x in chain:
        if itr == len(chain)-1: #adds last fragment if end of chain is reached
            token.append(x)
            fragments.append(''.join(token))
            itr+=1
        elif x in enzyme:   #adds fragment if enzyme is reached; starts new fragment
            token.append(x)
            fragments.append(''.join(token))
            token[:] = []
            itr+=1
        else:   #adds character to still-going fragment
            token.append(x)
            itr+=1
    return fragments


'''######################################################################################
Precondition: 
    fragments:  list(string) representing an RNA chain now fragmented via enzyme  
    enzyme: list(string) representing the enzyme being applied to the RNA chain; could be
            either G-enzyme or UC-enzyme
Postcondition:
    returns ext_base:
        list(list(string)) some RNA chain which has been doubly exposed to  two enzymes
        in a specified order
######################################################################################'''
def double_enzyme(fragments, enzyme):
    ext_base = [single_enzyme(frag, enzyme) for frag in fragments] #evaluates fragments as RNA chains themselves
    return ext_base