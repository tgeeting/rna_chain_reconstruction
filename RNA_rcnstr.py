from RNA_enzymes import *
from RNA_graphs import *

'''######################################################################################
Precondition:
    chains*:
        len(chains) == 1:   chains[0]:  string of a complete RNA chain
        len(chains) == 2:   chains[0]:  list[string] representing a RNA chain
                                        fragmented by the G-enzyme
                            chains[1]:  list[string] representing a RNA chain
                                    fragmented by the UC-enzyme
Postcondition:
    returns all_chains:
        all_chains: set[string] representing all possible RNA chains to be constructed
                    from the given fragments
                    If the set is empty, the original chain was an exception to this
                    method and the only obtainable 
######################################################################################'''
def RNA_reconstruction(*chains):
    if len(chains) == 1:
        G_fragments = single_enzyme(chains[0], ['G'])
        UC_fragments = single_enzyme(chains[0], ['U', 'C'])
    else:
        G_fragments, UC_fragments = chains[0], chains[1]
    ext_bases = double_enzyme(G_fragments, ['U', 'C']) + double_enzyme(UC_fragments, ['G'])
    RNA_ends = find_ends(ext_bases)
    RNA_graph = construct_RNA_graph(ext_bases, RNA_ends)
    all_chains = possible_RNA_chains(RNA_graph,RNA_ends[0], RNA_ends[0], len(''.join(G_fragments)))
    if len(all_chains) > 0:
        return all_chains
    else:
        return [chains[0]]