'''######################################################################################
AUTHOR: Travis Geeting
DATE:   05/12/15
DESC:   Program that generates all possible RNA chain reconstructions given a pair of
        enzyme fragmented chains or a complete RNA chain
RUN:    python RNA_main.py
######################################################################################'''
from RNA_display import menu, examples, generate_random, user_defined

if __name__ == "__main__":
    entry = ''
    while entry != 'Q':
        entry = menu()
        if entry == '1':
            examples()  #Example Output
        elif entry == '2':
            generate_random()   #Random Chain
        elif entry == '3':
            user_defined()  #User Chain
        elif entry == 'Q':
            break
        else:
            print "[INVALID COMMAND]"